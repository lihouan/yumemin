import store from '@/store/index.js'

export default function (that) {
     console.log('that11111111: ', that);
    return {
        color: "#161616",
        selectedColor: store.state.theme.presentTheme['--theme-color'],
        borderStyle: "black",
        backgroundColor: "#ffffff",
        list: [{
            pagePath: "/pages/home/index",
            iconPath: "/static/images/tabbar/home.png",
            selectedIconPath: "/static/images/tabbar/home_active.png",
            text: that.$t("pages.home"),
        }, {
            pagePath: "/pagesA/cMall/shoppingCart/index",
            iconPath: "/static/images/tabbar/shoppingCart.png",
            selectedIconPath: "/static/images/tabbar/shoppingCart_active.png",
            text:that.$t("cMall.shoppingCart"),
        }, {
            pagePath: "/pages/user/index",
            iconPath: "/static/images/tabbar/mine.png",
            selectedIconPath: "/static/images/tabbar/mine_active.png",
            text: that.$t("pages.mine"),
        }]
    }
}