import http from './interface'



export const applicationDetail = (params) => {
	return http.request({
		url: '/yumei-mall-pim/front/brand/authorization/application',
		method: 'get',
		params
	})
}

//所有等级列表
export const businessLevelAll = (params) => {
	return http.request({
		url: '/yumei-mall-pim/front/brand/level/business',
		method: 'get',
		params
	})
}

// 获取可选品牌列表
export const businessLevel = (params) => {
	return http.request({
		url: '/yumei-mall-pim/front/brand/level/business-by-application',
		method: 'get',
		params
	})
}

// 发起邀请时可选品牌
export const brandByInvite = (params) => {
	return http.request({
		url: '/yumei-mall-pim/front/brand/by-invite',
		method: 'get',
		params
	})
}