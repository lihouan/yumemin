import http from './interface'

// 确认订单
export const purchaseOrderConfirm = (data) => {
	return http.request({
		url: '/yumei-mall-purchase/front/purchase-order/confirm',
		method: 'post',
		data
	})
}

// 提交订单
export const purchaseOrderSubmit = (data) => {
	return http.request({
		url: '/yumei-mall-purchase/front/purchase-order/submit',
		method: 'post',
		data
	})
}

//获取支付方式
export const payType = (params) => {
	return http.request({
		url: '/yumei-mall-pay/front/pay/type',
		method: 'get',
		params
	})
}

//支付金额预算
export const skuAmountCalc = (data) => {
	return http.request({
		url: '/yumei-mall-pay/front/pay/sku-amount/calc',
		method: 'post',
		data
	})
}

//去支付
export const payParam = (data) => {
	return http.request({
		url: '/yumei-mall-pay/front/pay/param',
		method: 'post',
		data
	})
}

//验证银行卡快捷支付
export const validatePay = (data) => {
	return http.request({
		url: '/yumei-mall-pay/front/pay/validate',
		method: 'post',
		data
	})
}

//唤起密码控件设置密码
export const passwordControl = (data) => {
	return http.request({
		url: '/yumei-mall-pay/front/pay/password-control',
		method: 'post',
		data
	})
}

//各状态订单列表
export const purchaseOrderList = (params) => {
	return http.request({
		url: '/yumei-mall-purchase/front/purchase-order',
		method: 'get',
		params
	})
}

// 确认收货
export const purchaseOrderReceive = (id,params) => {
	return http.request({
		url: `/yumei-mall-purchase/front/purchase-order/${id}/receive`,
		method: 'get',
		params
	})
}

//查询订单详情
export const purchaseOrderDetails = (id,params) => {
	return http.request({
		url: '/yumei-mall-purchase/front/purchase-order/'+id,
		method: 'get',
		params
	})
}

//获取订单包裹列表
export const packagesList = (orderId,params) => {
	return http.request({
		url: `yumei-mall-purchase/front/purchase-package/${orderId}/packages`,
		method: 'get',
		params
	})
}

// 取消采购订单原因列表
export const purchaseOrderCancelReason = (params) => {
	return http.request({
		url: `/yumei-mall-purchase/front/purchase-order/cancel-reason`,
		method: 'get',
		params
	})
}

//取消订单
export const purchaseOrderCancel = (id,data) => {
	return http.request({
		url: `/yumei-mall-purchase/front/purchase-order/${id}/cancel`,
		method: 'post',
		data
	})
}

// 订单数量小红点
export const purchaseOrderCount = (params) => {
	return http.request({
		url: `/yumei-mall-purchase/front/purchase-order/count`,
		method: 'get',
		params
	})
}