import http from './interface'

// 获取微信jssdkConfig配置
export const getWechatConfig = (data) => {
	return http.request({
		url: '/yumei-mall-common/front/wechat/jsapi-ticket',
		method: 'get',
		data
	})
}
