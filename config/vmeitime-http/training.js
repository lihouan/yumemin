import http from './interface'
// 会议列表
export const meetList = (data) => {
	return http.request({
		url: '/yumei-mall-train/front/meet',
		method: 'get',
		data,
	})
}

// 我报名的
export const myTrain = (data) => {
	return http.request({
		url: '/yumei-mall-train/front/meet/my-train',
		method: 'get',
		data,
	})
}

//活动详情
export const meetDetails = (id,data) => {
	return http.request({
		url: `/yumei-mall-train/front/meet/${id}`,
		method: 'get',
		data,
	})
}

// 携带活动参与人
export const person = (id,data) => {
	return http.request({
		url: `/yumei-mall-train/front/involve/person/${id}`,
		method: 'get',
		data,
	})
}

// 活动报名
export const involve = (data) => {
	return http.request({
		url: `/yumei-mall-train/front/involve`,
		method: 'post',
		data
	})
}

// 取消报名
export const involveCancel = (data) => {
	return http.request({
		url: `/yumei-mall-train/front/involve/cancel`,
		method: 'get',
		data,
	})
}

// 货款明细
export const paymentDetails = (data) => {
	return http.request({
		url: `/yumei-mall-store/front/bank-roll/transaction`,
		method: 'get',
		data
	})
}


// 获取微信jssdkConfig配置
export const getWechatConfig = (data) => {
	return http.request({
		url: `/yumei-mall-common/front/wechat/jsapi-ticket`,
		method: 'get',
		data
	})
}

//签到会议扫码校验
export const scanCode = (id,data) => {
	return http.request({
		url: `/yumei-mall-train/front/meet/scan-code/${id}`,
		method: 'get',
		data
	})
}

