import http from './interface'

// 保证金规则列表接口
export const cashDepositRuleList = (params) => {
	return http.request({
		url: '/yumei-mall-store/front/cash-deposit-rule/list',
		method: 'get',
		params,
	})
}

//可划转金额资金显示明细接口
export const summaryByAccount = (params) => {
	return http.request({
		url: '/yumei-mall-store/front/bank-roll/online/summary-by-account',
		method: 'get',
		params,
	})
}

//确认划转获取支付方式
export const specifyType = (params) => {
	return http.request({
		url: '/yumei-mall-pay/front/recharge/specify-type?payCode=lianlian_balance_pay',
		method: 'get',
		params,
	})
}

// 获取充值支付要素
export const rechargeParam = (data) => {
	return http.request({
		url: '/yumei-mall-pay/front/recharge/param',
		method: 'post',
		data,
	})
}


//获取充值用途列表
export const rechargeList = (params) => {
	return http.request({
		url: '/yumei-mall-pim/front/brand/authorization/application/recharge-list',
		method: 'get',
		params,
	})
}

//获取充值收银方式列表
export const purchasetype = (params) => {
	return http.request({
		url: '/yumei-mall-pay/front/recharge/type',
		method: 'get',
		params,
	})
}


//查询最近的6S
export const nearestCenter = (params) => {
	return http.request({
		url: '/yumei-mall-customer/front/customer/nearest-center',
		method: 'get',
		params,
	})
}

//验证银行卡快捷支付
export const payValidate = (data) => {
	return http.request({
		url: '/yumei-mall-pay/front/pay/validate',
		method: 'post',
		data,
	})
}

// 线下打款所需信息
export const transferOfflineMsg = (data) => {
	return http.request({
		url: '/yumei-mall-pay/front/common/transferOfflineMsg',
		method: 'post',
		data,
	})
}

//查询用户充值结果
export const payInfo = (params) => {
	return http.request({
		url: '/yumei-mall-pay/front/recharge/pay-info',
		method: 'get',
		params,
	})
}

// 查询当前月份，是否有申请单
export const currentapply = (data) => {
	return http.request({
		url: '/yumei-mall-finance/front/customer/finance/currentapply',
		method: 'post',
		data
	})
}


//收益总览
export const incomeAmount = (params) => {
	return http.request({
		url: '/yumei-mall-finance/front/customer/income/amount',
		method: 'get',
		params,
	})
}

//提现明细
export const getIncomeDetailList = (params) => {
	return http.request({
		url: '/yumei-mall-finance/front/customer/income/getIncomeDetailList',
		method: 'get',
		params,
	})
}


//提现详情
export const withdrawInfot = (params) => {
	return http.request({
		url: '/yumei-mall-finance/front/customer/income/withdrawInfo',
		method: 'get',
		params,
	})
}

//提现记录查询
export const rewardOperationLog = (data) => {
	return http.request({
		url: '/yumei-mall-finance/front/customer/finance/rewardOperation/log',
		method: 'post',
		data
	})
}
