import Vue from 'vue'
import Vuex from 'vuex'
import loginInfo from './modules/login.js'
import theme from './modules/theme.js'
import basic from './modules/basic.js'

import  getters from './getters.js'
Vue.use(Vuex)
const store=new Vuex.Store({
	modules:{
		loginInfo,
		theme,
		basic
	},
	getters
})
export default store;