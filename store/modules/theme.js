import storage from '@/utils/storage.js'
let theme = {
    // 默认主题
    default: {
        '--bg-color': '#f6f6f6',
        '--theme-color': '#B41701'
    },
    // 定制主题
    custom: {
        '--bg-color': '#ccc',
        '--theme-color': 'blue'
    },
}
const state = {

    // 主题方案
    scheme: ['default', 'custom'],

    // 当前主题
    presentTheme: theme.default
}

const mutations = {
    UPDATE_THEME(state, {
        presentTheme
    }) {
        if (state.scheme.includes(presentTheme)) {
            state.presentTheme = theme.presentTheme
        }
    }
}

export default {
    state,
    mutations,
}