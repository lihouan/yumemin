import storage from '@/utils/storage.js'
import {
	getUserInfo,
} from '../../config/vmeitime-http/index'
const state = {
	userInfo: null,
	loginToken: storage.get('loginToken'),
	xTenant: storage.get('xTenant'), //测试环境请求头x-tenant值==》  mml -- 马来西亚 ， '' -- 国内
	storelevelData: storage.get('storelevelData'),  //当前平台数据
}

const mutations = {
	LOGIN_USERINFO(state, data) {
		state.userInfo = data
		storage.set('loginInfo', data)
	},
	LOGIN_TOKEN(state, data) {
		state.loginToken = data
		storage.set('loginToken', data)
	},
	X_TENANT(state, data) {
		console.log(data + '哈哈')
		state.xTenant = data
		storage.set('xTenant', data)
	},
    
	STORE_LEVEDATA(state, data) {
		state.storelevelData = data
		storage.set('storelevelData', data)
	},

}

const actions = {
	getUserInfo({
		commit,
		state,
	}) {
		return new Promise((resolve, reject) => {
			console.log('进来------------------------------------------')
			getUserInfo().then(res => {
				if (res) {
					commit('LOGIN_USERINFO', res)

					let storeleve=res?.storelevelList.lenght ?res.storelevelList[0]:{}
					
					if(!storage.get('storelevelData').storeId){
						commit('STORE_LEVEDATA',  storeleve)
					}
					console.log('state.storelevelData: ', state.storelevelData);
					resolve()
				}
			}).catch(err => {
				
				reject(err)
			})

		})
	},
	logout({
		commit,
	},ignoreType) {
		console.log('ignoreType: ', ignoreType);
		return new Promise((resolve, reject) => {
			storage.remove("inviteData");
			commit('LOGIN_USERINFO', '')
			commit('LOGIN_TOKEN', '')
			if(ignoreType!=='STORE_LEVEDATA'){
				commit('STORE_LEVEDATA', '')
			}
			
			resolve()
		})
	},
	setXTenant({
		commit,
	}, xTenant) {
		// let xTenant = 'mml'
		 commit('X_TENANT', xTenant || '')
	}

}

export default {
	state,
	mutations,
	actions,
}