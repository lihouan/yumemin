import App from './App'
import store from './store/index.js'
let i18nConfig = {
  locale: uni.getLocale()|| "zh-Hans",
  messages,
}
// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
import messages from '@/locale/index'
import api from './config/vmeitime-http'
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
export const i18n = new VueI18n(i18nConfig)
// 全局挂载后使用
Vue.prototype.$api = api
Vue.prototype.$store = store
//Vue.prototype.$jumpDomain = 'http://localhost:8080/#/'
Vue.prototype.$jumpDomain = 'https://h5-test.merrige.cn/H5/index.html#/'
const app = new Vue({
  mode:'hash',
  store,
  i18n,
  ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif