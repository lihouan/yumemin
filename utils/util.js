import Nav from './Nav'
import dayjs from '@/uni_modules/uv-ui-tools/libs/util/dayjs.js'

//手机号转换带*
export const convertRegPhone = (phone) => String(phone).slice(0, 3) + "******" + String(phone).slice(9);

const utils = {
	//格式化手机号码
	formatNumber: function(num) {
		return num.length === 11 ? num.replace(/^(\d{3})\d{4}(\d{4})$/, '$1****$2') : num;
	},
}


// 判断是否是微信内置浏览器
export function isWechat() {
	if (!navigator?.userAgent) {
		return false
	}
	return (
		String(navigator.userAgent.toLowerCase().match(/MicroMessenger/i)) ===
		"micromessenger"
	);
}


/**
 *
 * @param url 图片路径
 * @param ext 图片格式
 * @param callback 结果回调
 */
export function getUrlBase64(url, ext, height, width, callback) {
	var img = new Image,
		canvas = document.createElement("canvas"),
		ctx = canvas.getContext("2d"),
		src = url; // 具体的图片地址

	img.crossOrigin = "Anonymous";

	img.onload = function() {
		canvas.width = img.width;
		canvas.height = img.height;
		ctx.drawImage(img, 0, 0);
		callback.call(this, canvas.toDataURL("image/" + ext)); //回掉函数获取Base64编码
		canvas = null;
	}
	img.src = src;
	//  确保缓存的图片也触发 load 事件
	if (img.complete || img.complete === undefined) {
		img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
		img.src = src;
	}
}

export function showToast(title) {
	uni.showToast({
		title: title,
		icon: 'none',
		duration: 1500
	})
}


/**
 * 利用Canvas方式把图片URL转换为图片Base64字符串
 * @param imgUrl 图片URL
 * @param callback 回调函数异步接收图片Base64字符串
 */
export function urlToBase64ByCanvas(imgUrl, callback) {
	// const xhr = new XMLHttpRequest();
	// xhr.open('get', imgUrl, true);
	// xhr.responseType = 'blob';
	// xhr.onload = function () {
	// 	if (this.status == 200) {
	// 		const blob = this.response;
	// 		const oFileReader = new FileReader();
	// 		oFileReader.onloadend = function (e) {
	// 			if (e.target) {
	// 				const base64 = e.target.result;
	// 				if (callback) {
	// 					callback(base64);
	// 				}
	// 			}
	// 		};
	// 		oFileReader.readAsDataURL(blob);
	// 	}
	// };
	// xhr.send();
	const options = {
		responseType: 'blob',
		// mode:'no-cors',
		headers: {
			'Access-Control-Allow-Origin': '*',
			'Access-Control-Allow-Credentials': "true",
			'cache': "no-cache",
			'Cache-Control': 'no-cache'
		}
	};
	window.fetch(imgUrl, options)
		.then(response => {
			return response.blob();
		})
		.then(blob => {
			let oFileReader = new FileReader();
			oFileReader.onloadend = function(e) {
				if (e.target) {
					const base64 = e.target.result;
					if (callback) {
						callback(base64);
					}
				}
			};
			oFileReader.readAsDataURL(blob);
		});
}

// 将后端返回的城市数据结构转换成elementui的树数据结构
export const setTreeData = arr => {
	// 删除所有的children,以防止多次调用
	arr.forEach(function(item) {
		delete item.children
	})
	const map = {}
	arr.forEach(i => {
		map[i.code] = i
	})
	const treeData = []
	arr.forEach(child => {
		const mapItem = map[child.pCode] // 判断当前数据的pCode是否存在map中
		if (mapItem) {
			child.mapKey = {
				code: child.code,
				name: child.name
			};
			(mapItem.children || (mapItem.children = [])).push(child) // 这里判断mapItem中是否存在child
		} else {
			// 不存在则是顶层数据
			child.mapKey = {
				code: child.code,
				name: child.name
			}
			treeData.push(child)
		}
	})
	return treeData
}


// js计算精度
// 加法： accAdd(0.1, 0.2)  // 得到结果：0.3
export function accAdd(num1, num2) {
	num1 = Number(num1);
	num2 = Number(num2);
	var dec1, dec2, times;
	try {
		dec1 = countDecimals(num1) + 1;
	} catch (e) {
		dec1 = 0;
	}
	try {
		dec2 = countDecimals(num2) + 1;
	} catch (e) {
		dec2 = 0;
	}
	times = Math.pow(10, Math.max(dec1, dec2));
	// var result = (num1 * times + num2 * times) / times;
	var result = (accMul(num1, times) + accMul(num2, times)) / times;
	return getCorrectResult("add", num1, num2, result);
	// return result;
}

// 减法： accSub(1, 0.9)    // 得到结果：0.1
export function accSub(num1, num2) {
	num1 = Number(num1);
	num2 = Number(num2);
	let baseNum, baseNum1, baseNum2;
	try {
		baseNum1 = num1.toString().split(".")[1].length;
	} catch (e) {
		baseNum1 = 0;
	}
	try {
		baseNum2 = num2.toString().split(".")[1].length;
	} catch (e) {
		baseNum2 = 0;
	}
	baseNum = Math.pow(10, Math.max(baseNum1, baseNum2));
	return (num1 * baseNum - num2 * baseNum) / baseNum;
}

// 除法： accDiv(2.2, 100)  // 得到结果：0.022
export function accDiv(num1, num2) {
	num1 = Number(num1);
	num2 = Number(num2);
	var t1 = 0;
	var t2 = 0;
	var dec1;
	var dec2;
	try {
		t1 = countDecimals(num1);
		// eslint-disable-next-line no-empty
	} catch (e) {}
	try {
		t2 = countDecimals(num2);
		// eslint-disable-next-line no-empty
	} catch (e) {}
	dec1 = convertToInt(num1);
	dec2 = convertToInt(num2);
	// console.log("除法=========resultresultresult==",dec1,dec2,t2,t1)
	var result = accMul(dec1 / dec2, Math.pow(10, t2 - t1));

	return getCorrectResult("div", num1, num2, result);
	// return result;
}

// 乘法： accMul(7, 0.8) 0.01 1    // 得到结果：5.6
export function accMul(num1, num2) {
	num1 = Number(num1);
	num2 = Number(num2);
	var times = 0;
	var s1 = num1.toString();
	var s2 = num2.toString();
	try {
		times += countDecimals(s1);
		// eslint-disable-next-line no-empty
	} catch (e) {}
	try {
		times += countDecimals(s2);
		// eslint-disable-next-line no-empty
	} catch (e) {}

	var result = (convertToInt(s1) * convertToInt(s2)) / Math.pow(10, times);

	// console.log("乘法=========resultresultresult==",convertToInt(s1),convertToInt(s2), Math.pow(10, times),"====",result)

	return getCorrectResult("mul", num1, num2, result);
	// return result;
}
// convertToInt()方法：将小数转成整数
var convertToInt = function(num) {
	num = Number(num);
	var newNum = num;
	var times = countDecimals(num);
	var tempNum = num.toString().toUpperCase();
	if (tempNum.split("E").length === 2) {
		newNum = Math.round(num * Math.pow(10, times));
	} else {
		newNum = Number(tempNum.replace(".", ""));
	}
	return newNum;
};
// countDecimals()方法：计算小数位的长度
var countDecimals = function(num) {
	var len = 0;
	try {
		num = Number(num);
		var str = num.toString().toUpperCase();
		if (str.split("E").length === 2) {
			// scientific notation
			var isDecimal = false;
			if (str.split(".").length === 2) {
				str = str.split(".")[1];
				if (parseInt(str.split("E")[0]) !== 0) {
					isDecimal = true;
				}
			}
			const x = str.split("E");
			if (isDecimal) {
				len = x[0].length;
			}
			len -= parseInt(x[1]);
		} else if (str.split(".").length === 2) {
			// decimal
			// eslint-disable-next-line no-empty
			if (parseInt(str.split(".")[1]) !== 0) {}
		}
		// eslint-disable-next-line no-useless-catch
	} catch (e) {
		throw e;
	} finally {
		if (isNaN(len) || len < 0) {
			len = 0;
		}
	}
	return len;
};
export function formatDecimal(num, len) {
	let arr = num.toString().split(".");
	if (arr.length > 1) {
		return arr[0] + "." + arr[1].slice(0, len);
	}
	return arr[0];
}
// getCorrectResult()方法：确认我们的计算结果无误，以防万一
// eslint-disable-next-line no-unused-vars
var getCorrectResult = function(type, num1, num2, result) {
	// console.log(type, num1, num2, result)
	var tempResult = 0;
	switch (type) {
		case "add":
			tempResult = num1 + num2;
			break;
		case "sub":
			tempResult = num1 - num2;
			break;
		case "div":
			tempResult = num1 / num2;
			// console.log("tempResult============>",result,tempResult,)
			break;
		case "mul":
			tempResult = num1 * num2;

			break;
	}
	// if (Math.abs(result - tempResult) > 0.08) {
	// 	console.log("取了tempResult====》》》",tempResult)
	// 	return tempResult
	// }
	// console.log("取了result====》》》》",result)
	return tempResult; // result
};

/**
 * @name 价格过滤
 * @param {Number} value 值
 * @param {Number} decimals 位数
 */
const digitsRE = /(\d{3})(?=\d)/g;

export function currency(value, currency, decimals) {
	if (!value) {
		return "0.00";
	}
	value = accDiv(parseFloat(value), 1); // 金额统一除以100
	if (!isFinite(value) || (!value && value !== 0)) return "";
	currency = currency != null ? currency : "";
	decimals = decimals != null ? decimals : 2;
	let stringified = Math.abs(value).toFixed(decimals);
	let _int = decimals ? stringified.slice(0, -1 - decimals) : stringified;
	let i = _int.length % 3;
	let head = i > 0 ? _int.slice(0, i) + (_int.length > 3 ? "," : "") : "";
	let _float = decimals ? stringified.slice(-1 - decimals) : "";
	let sign = value < 0 ? "-" : "";
	let currencyData =
		sign +
		currency +
		head +
		_int.slice(i).replace(digitsRE, function(val) {
			return val + ",";
		}) +
		_float;
	return fill(currencyData);
}

/**
 * @name 过滤
 * @param {String} val 值
 * @param {String} de 填充值
 */
export function fill(val, de) {
	const cur = de;
	return val || val === 0 ? val : de ? cur : "--";
}
// 防抖
export function debounceImmediate(fn, delay, immediate = false) {
	// 1.定义一个定时器, 保存上一次的定时器
	let timer = null
	let isInvoke = false

	// 2.真正执行的函数
	const _debounce = function(...args) {
		// 取消上一次的定时器
		if (timer) clearTimeout(timer)

		// 判断是否需要立即执行
		if (immediate && !isInvoke) {
			if (typeof(fn) == 'string') {
				this[fn].apply(this, args)
			} else {
				fn.apply(this, args)
			}
			isInvoke = true
		} else {
			// 延迟执行
			timer = setTimeout(() => {
				// 外部传入的真正要执行的函数
				if (typeof(fn) == 'string') {
					this[fn].apply(this, args)
				} else {
					fn.apply(this, args)
				}
				isInvoke = false
			}, delay)
		}
	}

	return _debounce
}

const nav = new Nav('uniApp')
export const NavPush = function(url, openType) {

	nav.jumpTo(url, openType)

}

export function noVcode(that) {
	console.log('that: ', that);

	uni.showModal({
		title: that.$t("register.nodeVerificationCode"),
		confirmColor: that.$store.state.theme.presentTheme["--theme-color"],
		showCancel: false,
		success: function(res) {
			if (res.confirm) {
				console.log("用户点击确定");
			} else if (res.cancel) {
				console.log("用户点击取消");
			}
		},
	});
	setTimeout(() => {
		document.getElementsByClassName('uni-modal__bd')[0].innerHTML =
			`<div style='text-align: left; font-size: 13px;'><p>${that.$t("noVcode.info1")}</p><p>${that.$t("noVcode.info2")}</p><p>${that.$t("noVcode.info3")}</p><p>${that.$t("noVcode.info4")}</p><p>${that.$t("noVcode.info5")}</p></div>`
	}, 100)

}

//获取所有链接参数
export const getUrlQuery = function(url, decode) {
	decode = decode === false ? false : true
	url = url ? url : window.location.href
	url = url.replace('#/', '')
	// url = decodeURIComponent(url)
	let tA = url.split('?'),
		_return = {};
	if (tA[1]) {
		let tB = tA[1].split('&');
		tB.forEach((v, i) => {
			if (v) {
				let tC = v.split('=');
				_return[tC[0]] = tC[1] && tC[1] != 'undefined' ? (decode ? decodeURIComponent(tC[1]) : tC[
					1]) : ''
			}
		})
	}
	return _return;
}

// 复制富文本处理
export const convertHtmlToText = function(inputText) {
	var returnText = "" + inputText;
	returnText = returnText.replace(/<\/div>/ig, '\r\n');
	returnText = returnText.replace(/<\/li>/ig, '\r\n');
	returnText = returnText.replace(/<li>/ig, '  *  ');
	returnText = returnText.replace(/<\/ul>/ig, '\r\n');
	//-- remove BR tags and replace them with line break
	returnText = returnText.replace(/<br\s*[\/]?>/gi, "\r\n");

	//-- remove P and A tags but preserve what's inside of them
	returnText = returnText.replace(/<p.*?>/gi, "\r\n");
	returnText = returnText.replace(/<a.*href="(.*?)".*>(.*?)<\/a>/gi, " $2 ($1)");

	//-- remove all inside SCRIPT and STYLE tags
	returnText = returnText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
	returnText = returnText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
	//-- remove all else
	returnText = returnText.replace(/<(?:.|\s)*?>/g, "");

	//-- get rid of more than 2 multiple line breaks:
	returnText = returnText.replace(/(?:(?:\r\n|\r|\n)\s*){2,}/gim, "\r\n\r\n");

	//-- get rid of more than 2 spaces:
	returnText = returnText.replace(/ +(?= )/g, '');

	//-- get rid of html-encoded characters:
	returnText = returnText.replace(/ /gi, " ");
	returnText = returnText.replace(/&/gi, "&");
	returnText = returnText.replace(/"/gi, '"');
	returnText = returnText.replace(/</gi, '<');
	returnText = returnText.replace(/>/gi, '>');
	returnText = returnText.replace(/\r\n\r\n/gi, '\r\n');

	return returnText.trim();
}


export function getTimeRange(type) {
	let start = ''
	let end = ''
	switch (type) {
		case '本年':
			start = dayjs().startOf('year').format('YYYY-MM-DD');
			end = dayjs().endOf('year').format('YYYY-MM-DD');
			break;
		case '上年':
			start = dayjs().add(-1, 'year').startOf('year').format('YYYY-MM-DD');
			end = dayjs().add(-1, 'year').endOf('year').format('YYYY-MM-DD');
			break;
		case '下年':
			start = dayjs().add(1, 'year').startOf('year').format('YYYY-MM-DD');
			end = dayjs().add(1, 'year').endOf('year').format('YYYY-MM-DD');
			break;
		case '上半年':
			start = dayjs().startOf('year').format('YYYY-MM-DD');
			end = dayjs().endOf('year').subtract(6, 'month').format('YYYY-MM-DD');
			break;
		case '下半年':
			start = dayjs().startOf('year').add(6, 'month').format('YYYY-MM-DD');
			end = dayjs().endOf('year').format('YYYY-MM-DD');
			break;
		case '本季度':
			start = dayjs().startOf('quarter').format('YYYY-MM-DD');
			end = dayjs().endOf('quarter').format('YYYY-MM-DD');
			break;
		case '上季度':
			start = dayjs().add(-1, 'quarter').startOf('quarter').format('YYYY-MM-DD');
			end = dayjs().add(-1, 'quarter').endOf('quarter').format('YYYY-MM-DD');
			break;
		case '下季度':
			start = dayjs().add(1, 'quarter').startOf('quarter').format('YYYY-MM-DD');
			end = dayjs().add(1, 'quarter').endOf('quarter').format('YYYY-MM-DD');
			break;
		case 'current month': //本月':
			start = dayjs().startOf('month').format('YYYY-MM-DD');
			end = dayjs().endOf('month').format('YYYY-MM-DD');
			break;
		case 'last month': //上月
			start = dayjs().add(-1, 'month').startOf('month').format('YYYY-MM-DD');
			end = dayjs().add(-1, 'month').endOf('month').format('YYYY-MM-DD');
			break;
		case '下月':
			start = dayjs().add(1, 'month').startOf('month').format('YYYY-MM-DD');
			end = dayjs().add(1, 'month').endOf('month').format('YYYY-MM-DD');
			break;
		case "Last 3 Months": //'近3月':
			start = dayjs().subtract(3, 'month').format('YYYY-MM-DD');
			end = dayjs().format('YYYY-MM-DD');
			break;
		case "Last 6 Months": //近6个月
			start = dayjs().subtract(6, 'month').format('YYYY-MM-DD');
			end = dayjs().format('YYYY-MM-DD');
			break;
		case '本周':
			start = dayjs().startOf('week').add(1, 'day').format('YYYY-MM-DD');
			end = dayjs().endOf('week').add(1, 'day').format('YYYY-MM-DD');
			break;
		case '上周':
			start = dayjs().add(-1, 'week').startOf('week').add(1, 'day').format('YYYY-MM-DD');
			end = dayjs().add(-1, 'week').endOf('week').add(1, 'day').format('YYYY-MM-DD');
			break;
		case '下周':
			start = dayjs().add(1, 'week').startOf('week').add(1, 'day').format('YYYY-MM-DD');
			end = dayjs().add(1, 'week').endOf('week').add(1, 'day').format('YYYY-MM-DD');
			break;
		case "today": //'今日':
			start = dayjs().format('YYYY-MM-DD');
			end = dayjs().format('YYYY-MM-DD');
			break;
		case "yesterday": //'昨日':
			start = dayjs().add(-1, 'day').format('YYYY-MM-DD');
			end = dayjs().add(-1, 'day').format('YYYY-MM-DD');
			break;
		case '明天':
			start = dayjs().add(1, 'day').format('YYYY-MM-DD');
			end = dayjs().add(1, 'day').format('YYYY-MM-DD');
			break;
		case "Last 7 days": //'过去7天':
			start = dayjs().add(-7, 'day').format('YYYY-MM-DD');
			end = dayjs().format('YYYY-MM-DD');
			break;
		case '过去30天':
			start = dayjs().add(-30, 'day').format('YYYY-MM-DD');
			end = dayjs().format('YYYY-MM-DD');
			break;
		case '未来7天':
			start = dayjs().format('YYYY-MM-DD');
			end = dayjs().add(7, 'day').format('YYYY-MM-DD');
			break;
		case '未来30天':
			start = dayjs().format('YYYY-MM-DD');
			end = dayjs().add(30, 'day').format('YYYY-MM-DD');
			break;
		default:
	}
	return [start, end];
}

export default {
	formatNumber: utils.formatNumber,
}