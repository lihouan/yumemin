//门店申请数据
export const applyRules = {
    physicalType: {
        required: true,
        message: '请选择店铺位置',
        trigger: ['blur', 'change']
    },
    "address.address": {
        type: 'string',
        required: true,
        message: '请选择地址',
        trigger: ['blur', 'change']
    },
    storeName: {
        type: 'string',
        required: true,
        message: '输入店名',
        trigger: ['blur', 'change']
    },
    storePhone: {
        type: 'string',
        required: true,
        message: '输入联系方式',
        trigger: ['blur', 'change']
    },
    licPictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    doorHeadPictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    displayPictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    transferAccountPictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    marriageCertificatePictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
}

// 优秀实体门店申请
export const goodApplyRules = {
    "address.address": {
        type: 'string',
        required: true,
        message: '请选择地址',
        trigger: ['blur', 'change']
    },
    storeName: {
        type: 'string',
        required: true,
        message: '请输入',
        trigger: ['blur', 'change']
    },
}

// 生活馆申请
export const lifeHouseRules = {
    "address.address": {
        type: 'string',
        required: true,
        message: '请选择地址',
        trigger: ['blur', 'change']
    },
    territory: {
        type: 'string',
        required: true,
        message: '请选择',
        trigger: ['blur', 'change']
    }, //"地盘位置 ：Street 临街 Mall 商场",
    storeArea: {
        type: 'string',
        required: true,
        message: '请输入',
        trigger: ['blur', 'change']
    }, //"门店面积",
    buildHeight: {
        type: 'string',
        required: true,
        message: '请输入',
        trigger: ['blur', 'change']
    }, //: "楼高",
    idCardFrontPictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    idCardOppositePictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    licPictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    planeSizePictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    doorHeadPictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    inStorePictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    insideStorePictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
}

export const lifePavilionCheckDataRules = {
    doorHeadPictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    inStorePictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    insideStorePictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    imageWallPictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    fittingRoomPictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
    showcasePictureUrl: {
        type: 'string',
        required: true,
        message: '请上传图片',
        trigger: ['blur', 'change']
    },
}

// 教师申请
export const teacherTrainingRules = {
    "phone": {
        type: 'string',
        required: true,
        message: '请输入',
        trigger: ['blur', 'change']
    }, // "用户电话",
    "purpose": {
        type: 'string',
        required: true,
        message: '请输入',
        trigger: ['blur', 'change']
    }, // "培训目的",
    "userName": {
        type: 'string',
        required: true,
        message: '请输入',
        trigger: ['blur', 'change']
    },

    "address.address": {
        type: 'string',
        required: true,
        message: '请选择地址',
        trigger: ['blur', 'change']
    },
}

export function openAccountRulesFn(that) {
    return {
        realName: {
            type: "string",
            required: true,
            message: that.$t("register.pleaseEnter"),
            trigger: ["blur", "change"],
        },
        type: {
            type: "string",
            required: true,
            message: that.$t("authentication.PleaseSelect"),
            trigger: ["blur", "change"],
        },
        credentialsType: {
            type: "string",
            required: true,
            message: "请选择",
            trigger: ["blur", "change"],
        },
        customerIdCardNum: {
            type: "string",
            required: true,
            message: "请输入",
            trigger: ["blur", "change"],
        },
        idCardExpiredDate: {
            type: "string",
            required: true,
            message: "请输入",
            trigger: ["blur", "change"],
        },
        mobile: [{
            required: true,
            message: '请输入',
            trigger: ['change', 'blur'],
        }, {
            validator: (rule, value, callback) => {
                // 上面有说，返回true表示校验通过，返回false表示不通过
                // uni.$u.test.mobile()就是返回true或者false的
                return uni.$u.test.mobile(value);
            },
            message: '手机号码不正确',
            // 触发器可以同时用blur和change
            trigger: ['change', 'blur'],
        }],
        currentCity: {
            required: true,
            message: "请选择",
            trigger: ["blur", "change"],
        },
        "address.address": {
            type: 'string',
            required: true,
            message: '请输入',
            trigger: ['blur', 'change']
        },
        email: [{
            required: true,
            message: '请输入',
            trigger: ['change', 'blur'],
        }, {
            validator: (rule, value, callback) => {
                return uni.$u.test.email(value);
            },
            message: '邮箱格式不正确',
            // 触发器可以同时用blur和change
            trigger: ['change', 'blur'],
        }],
        idCardFrontUrl: {
            type: 'string',
            required: true,
            message: '请上传图片',
            trigger: ['blur', 'change']
        },
        idCardBackUrl: {
            type: 'string',
            required: true,
            message: '请上传图片',
            trigger: ['blur', 'change']
        },
        creditCodeUrl: {
            type: 'string',
            required: true,
            message: '请上传图片',
            trigger: ['blur', 'change']
        },
        orgType:{
            type: "string",
            required: true,
            message: "请选择",
            trigger: ["blur", "change"],
        },
        creditCode:{
            type: "string",
            required: true,
            message: "请输入",
            trigger: ["blur", "change"],
        },
        bankName:{
            type: "string",
            required: true,
            message: "请选择",
            trigger: ["blur", "change"],
        },
        bankCardNum:{
            type: "string",
            required: true,
            message: "请输入",
            trigger: ["blur", "change"],
        },
        orgName:{
            type: "string",
            required: true,
            message: "请输入",
            trigger: ["blur", "change"],
        },
        industryCode:{
            type: "string",
            required: true,
            message: "请选择",
            trigger: ["blur", "change"],
        },
    }

}