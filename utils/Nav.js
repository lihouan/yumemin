/**
 * 公用路由解析方法
 * @param env app || wxapp
 */
class Nav {
    constructor(env, CONFIG) {
        this.env = env
        this.replaceStartMap = {
            '/dtr/': {
                port: '8088'
            },
            '/h5_old/': {
                port: '8088'
            },
            '/h5/': {
                port: '8080'
            },
            '/act/': {
                port: '8181'
            }
        }
        //判定运行环境
        if (this.env == 'app') {
            let u = navigator.userAgent;
            if ((u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) && u.indexOf('TST_APP') > -1) {
                this.pType = 'android';
            } else if (!!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/) && u.indexOf('TST_APP') > -1) {
                this.pType = 'ios';
            } else if (u.indexOf('miniprogram') > -1 || window.__wxjs_environment === 'miniprogram') {
                this.pType = 'miniprogram';
                this.isWxapp = true
            } else {
                this.pType = 'browser';
                this.isBrowser = true
            }
            if (window.location.origin.indexOf('127.0') == -1 && window.location.origin.indexOf('localhost') == -1 && window.location.origin.indexOf(':8080') == -1) {
                this.isDevelopment = false
            } else {
                this.isDevelopment = true
            }

            this.prefix = process.env.VUE_APP_HTML_PREFIX || '/h5'
            this.origin = window.location.origin + (this.isDevelopment ? '' : this.prefix)
        } else if (this.env == 'wxapp') {
            this.prefix = '/h5'
            setTimeout(() => {
                this.CONFIG = getApp ? getApp().globalData.CONFIG : ''
            }, 1000)
        }

        this.requestMapData().then(res => {
            this.mapData = res
        })
    }
    /**
     * 获取地址映射数据
     */
    requestMapData() {
        return new Promise(resolve => {
            let MapData = {

                'h5InApp': {
                    'goods/detail': '${origin}/product-detail.html?${query}',
                    'product-detail/product-detail': '${origin}/product-detail.html?${query}',
                    'public/public': '${origin}/custom.html?${query}',
                    'custom/custom': '${origin}/custom.html?${query}',
                    'classify/classify': '${origin}/classify.html?${query}',
                    'product-list/product-list': '${origin}/product-list.html?${query}',
                    'pages-video/detail/detail': 'callHandler|goToShortVideoDetailPage|id,',
                    'pages-video/detail-picture/detail-picture': 'callHandler|goToShortVideoPicDetailPage|id,',
                    'live/live': 'callHandler|goToLiveRoom|liveNumber:number,',
                    'pages-video/add/add': 'callHandler|goToAddShortVideoPage|reference,',
                    'PLAYVIDEO?': '/video.html?${query}:QM_url:path,', //:QM  query 映射 query
                    'pages-main/index/index': 'callHandler|goToHomePage|type=1',
                    'pages-main/tbt-index/tbt-index': 'callHandler|goToHomePage|type=2',
                    'pages-main/live/index': 'callHandler|goToHomePage|type=3',
                    'pages-main/short-video/index': 'callHandler|goToHomePage|type=4',
                    'pages-main/account/account': 'callHandler|goToHomePage|type=5',
                    'CONFIRMORDER?': '${origin}/confirm-order.html?${query}',
                },
                'h5InWxapp': {
                    'product-detail.html': '/pages-goods/product-detail/product-detail?${query}',
                    'login.html': '/pages-native/auth/auth?next=back&${query}',
                    'coupon-list.html': '/pages-user/coupon-list/coupon-list?${query}',
                    'public/public': '/pages-main/custom/custom?${query}',
                    'custom/custom': '/pages-main/custom/custom?${query}',
                    'classify/classify': '/pages-goods/classify/classify?${query}',
                    'pages/live/live': '/pages-live/live/live?${query}',
                    'product-list/product-list': '/pages-goods/product-list/product-list?${query}',
                    'product-list.html': '/pages-goods/product-list/product-list?${query}',
                    'pages/video/video': '/pages-live/video/video?${query}',
                    'goods/detail': '/pages-goods/product-detail/product-detail?${query}',
                    'PLAYVIDEO?': '/video.html?${query}:QM_url:path,', //:QM  query 映射 query
                    'CONFIRMORDER?': '/pages-order/confirm-order/confirm-order?${query}',
                    'custom.html': '/pages-main/custom/custom?${query}',
                    'comment-assess.html': '/pages-native/order-comment/order-comment?${query}',
                    'order-list.html': '/pages-native/order-list/order-list?${query}',
                    'confirm-skill.html': '/pages-order/confirm-skill/confirm-skill?${query}'
                },
                'inWxapp': {
                    'product-detail.html': '/pages-goods/product-detail/product-detail?${query}',
                    'coupon-list.html': '/pages-user/coupon-list/coupon-list?${query}',
                    'custom.html': '/pages-main/custom/custom?${query}',
                    'public/public': '/pages-main/custom/custom?${query}',
                    'custom/custom': '/pages-main/custom/custom?${query}',
                    'classify/classify': '/pages-goods/classify/classify?${query}',
                    'pages/live/live': '/pages-live/live/live?${query}',
                    'product-list/product-list': '/pages-goods/product-list/product-list?${query}',
                    'product-list.html': '/pages-goods/product-list/product-list?${query}',
                    'pages/video/video': '/pages-live/video/video?${query}',
                    'goods/detail': '/pages-goods/product-detail/product-detail?${query}',
                    'PLAYVIDEO?': 'callHandler|previewMedia|url:path,poster',
                    'CONFIRMORDER?': '/pages-order/confirm-order/confirm-order?${query}',
                    'order-list.html': '/pages-native/order-list/order-list?${query}',
                    'h5/confirm-skill.html': 'pages-order/confirm-skill/confirm-skill?${query}'

                }
            }
            resolve(MapData)
        })
    }

    /**
     * 不同平台跳转
     */
    platformJumpData(url) {
        let jumpUrls = {
            '/product-detail.html': {
                'wholesale': '/helpSell-detail.html'
            }
        }
        const curUrlArr = url.split('?')
        const curOptions = this.getUrlQuery(url)
        const {
            platform
        } = curOptions
        const curJumObj = jumpUrls[curUrlArr[0]] ? jumpUrls[curUrlArr[0]] : url
        const realUrl = platform && curJumObj[platform] ? curJumObj[platform] + `${curUrlArr[1] ? '?' + curUrlArr[1] : ''}` : url
        return realUrl
    }

    /**
     * 获取缓存
     */
    getStorage(key) {
        if (this.env == 'app') {
            return window.localStorage.getItem(key)
        } else if (this.env == 'wxapp') {
            return wx.getStorageSync(key)
        }
    }
    /**
     * 设置缓存
     */
    setStorage(key, value) {
        if (this.env == 'app') {
            window.localStorage.setItem(key, value)
        } else if (this.env == 'wxapp') {
            wx.setStorageSync(key, value)
        }
    }
    /**
     * 检查是否为在小程序中打开的H5项目
     * */
    checkIsWxapp() {
        return new Promise((resolve) => {
            let storage = this.getStorage('isWxapp'),
                timer = ''
            if (storage) {
                resolve(storage == 'true' ? true : false)
                return
            }

            function ready() {
                let isWxapp = (window.__wxjs_environment === 'miniprogram')
                this.setStorage('isWxapp', isWxapp)
                clearTimeout(timer)
                resolve(isWxapp)
            }
            if (!window.WeixinJSBridge || !WeixinJSBridge.invoke) {
                document.addEventListener('WeixinJSBridgeReady', ready, false)

            } else {
                ready()
            }
            timer = setTimeout(() => {
                resolve(false)
            }, 300)

        })

    }

    /**
     * h5中执行小程序的跳转
     */
    miniProgram(navigateType, url) {
        if (window.wx) {
            window.wx.miniProgram[navigateType]({
                url: url
            })
        } else {
            setTimeout(() => {

                window.wx.miniProgram[navigateType]({
                    url: url
                })
            }, 1000)
        }
    }
    navigateTo(url) {
        if (this.env == 'app') {


            if (this.isWxapp) {
                this.miniProgram('navigateTo', url)
            } else if (this.isBrowser) {
                console.log('is isBrowser')
                window.location.href = url
            } else if (this.pType == 'android' || this.pType == 'ios') {
                // window.WebViewJavascriptBridge.callHandler('createNewWebView', { 'url': url }, function (response) {
                //     console.log('createNewWebView response', response)
                // })   
                if (url.indexOf('/dtr/') != -1 || url.indexOf('isDistribution=true') != -1) {
                    window.flutter_inappwebview.callHandler("navCreateWebView", {
                        url: url,
                        showSafeAreaTop: true,
                        containUrlParams: true,
                    }).then(res => {
                        console.log('res')
                        console.log(JSON.stringify(res))
                    });

                } else {
                    window.flutter_inappwebview.callHandler("navCreateWebView", {
                        url: url,
                    }).then(res => {
                        console.log('res')
                        console.log(JSON.stringify(res))
                    });
                }
            } else {
                console.log('无法识别的设备类型')
            }
        } else if (this.env == 'wxapp') {
            wx['navigateTo']({
                url: url
            })
        } else if (this.env == "uniApp") {

            uni['navigateTo']({
                url: url
            })
        }
    }

    redirectTo(url) {
        console.log("redirectTo");
        if (this.env == 'app') {
            if (this.isWxapp) {
                if (window.location.href.indexOf('confirm-order.html') != -1) {
                    this.setStorage('replaceBeforeUrlOrder', window.location.href)
                } else if (window.location.href.indexOf('order-detail') != -1) {
                    this.setStorage('replaceBeforeUrlOrderDetail', window.location.href)
                } else {
                    this.setStorage('replaceBeforeUrl', window.location.href)
                }
                this.miniProgram('redirectTo', url)
            } else if (this.isBrowser || this.pType == 'android' || this.pType == 'ios') {
                if (window.location.href.indexOf('confirm-order.html') != -1) {
                    this.setStorage('replaceBeforeUrlOrder', window.location.href)
                } else if (window.location.href.indexOf('order-detail.html') != -1) {
                    this.setStorage('replaceBeforeUrlOrderDetail', window.location.href)
                } else {
                    this.setStorage('replaceBeforeUrl', window.location.href)
                }
                window.location.replace(url)
            } else {
                console.log('无法识别的设备类型')
            }
        } else if (this.env == 'wxapp') {
            wx['redirectTo']({
                url: url
            })
        } else if (this.env == "uniApp") {
            uni['navigateTo']({
                url: url
            })
        }
    }
    switchTab(url) {
        if (this.env == 'app') {
            if (this.isWxapp) {
                this.miniProgram('switchTab', url)
            } else if (this.isBrowser) {
                window.location.replace(url)
            } else if (this.pType == 'android' || this.pType == 'ios') {
                console.log('无法匹配的Tab页')
            } else {
                console.log('无法识别的设备类型')
            }
        } else if (this.env == 'wxapp') {
            wx['switchTab']({
                url: url
            })
        } else if (this.env == "uniApp") {
            uni['navigateTo']({
                url: url
            })
        }
    }
    /**
     * 获取原始跳转链接和跳转方式
     */
    getSourceUrlAndOpenType(urlOrEvent, type) {
        console.log('urlOrEvent,  ', urlOrEvent, "type:", type);
        let _return = {
                url: '',
                openType: ''
            },
            mustLogin = false
        if (this.env == 'app' || this.env == 'uniApp') {
            //_return.url = urlOrEvent
            _return.url = this.platformJumpData(urlOrEvent)
            type = type == 'current' ? 'redirectTo' : type
            _return.openType = type
            mustLogin = urlOrEvent.indexOf('mustLogin=') != -1 ? true : false
            _return.mustLogin = mustLogin || ''
        } else if (this.env == 'wxapp') {
            let dataset = urlOrEvent.currentTarget.dataset
            _return.url = this.platformJumpData(dataset.url) || ''
            mustLogin = this.platformJumpData(dataset.url).indexOf('mustLogin=') != -1 ? true : false
            _return.openType = dataset.opentype
            _return.mustLogin = dataset.mustlogin || mustLogin || ''
        }
        _return.openType = _return.openType || 'navigateTo'
        console.log('_return---->', _return)
        return _return
    }
    //获取所有链接参数
    getUrlQuery(url) {
        url = url ? url : window.location.href
        url = decodeURIComponent(url)
        let tA = url.split('?'),
            _return = {};
        if (tA[1]) {
            let tB = tA[1].split('&');
            tB.forEach((v, i) => {
                if (v) {
                    let tC = v.split('=');
                    _return[tC[0]] = tC[1] && tC[1] != 'undefined' ? tC[1] : ''
                }
            })
        }
        return _return;
    }
    /**
     * 映射各环境平台的链接
     */
    mapUrl(url) {
        let MapData = this.mapData,
            webVersion = this.getStorage('webVersion')
        url = url.replace(/\amp;/g, '');

        //一些不用匹配的链接 app 中的.html 类型的链接  小程序中 pages-开头的链接
        if ((url.indexOf('.html') != -1 || url.indexOf('http') == 0) && this.env == 'app' && !this.isWxapp) {
            console.log('001')
            if (url.indexOf('http') != 0) {
                let replaceKey = '',
                    replacePort = ''
                for (let k in this.replaceStartMap) {
                    let v = this.replaceStartMap[k];
                    if (url.indexOf(k) == 0) {
                        replaceKey = k
                        replacePort = v.port
                        break;
                    }
                }
                if (replaceKey) {
                    if (process.env.NODE_ENV == "development") {
                        url = (window.location.origin.replace(':' + window.location.port, ':' + replacePort)) + url.replace(replaceKey, '/')
                    } else {
                        url = this.origin.replace(this.prefix, '') + url
                    }
                } else {
                    url = this.origin + (url.indexOf('/') == 0 ? '' : '/') + url
                }
            }
        } else if (url.indexOf('pages-main/product-list') == -1 && (url.indexOf('pages-') != -1 || url.indexOf('packageWolai') != -1 || url.indexOf('packageLive') != -1) && (this.env == 'wxapp' || this.isWxapp)) {
            url = (url.indexOf('/') == 0 ? '' : '/') + url
        } else {
            let mapKey = 'h5InApp',
                isMatch = false;
            if (this.env == 'app' && this.isWxapp) {
                mapKey = 'h5InWxapp'
            } else if (this.env == 'wxapp') {
                mapKey = 'inWxapp'
            }
            // console.log('mapKey',mapKey)
            let map = MapData[mapKey]
            for (let k in map) {
                // console.log('k', k)
                if (url.indexOf(k) != -1) {
                    // console.log('match', k, url)
                    url = this.matchUrlRex(url, map[k])
                    isMatch = true
                    break
                }
            }
            if (!isMatch) {
                if (this.env == 'wxapp' || (this.env == 'app' && this.isWxapp)) {
                    let origin = this.env == 'wxapp' ? this.CONFIG.webviewUrl : this.origin
                    if (url.indexOf('http') != 0 && url.indexOf('.html') != -1) {
                        let replaceKey = '',
                            replacePort = ''
                        for (let k in this.replaceStartMap) {
                            let v = this.replaceStartMap[k];
                            if (url.indexOf(k) == 0) {
                                replaceKey = k
                                replacePort = v.port
                                break;
                            }
                        }
                        if (replaceKey) {
                            url = encodeURIComponent(origin.replace(this.prefix, '') + url)
                        } else {
                            url = encodeURIComponent(origin + ((url.indexOf('/') == 0 ? '' : '/') + url))
                        }
                    } else if (url.indexOf('http') == 0) {
                        url = encodeURIComponent(url)
                    }

                    url = '/pages-native/webview/webview?url=' + url
                }
            }

        }
        if (!url) {
            return ''
        }
        if (webVersion) {
            let urlPath = url.split('?')[0],
                urlQueryStr = url.split('?')[1]
            urlQueryStr += ('&webVersion=' + webVersion)
            url = urlPath + '?' + (urlQueryStr)

        }
        // console.log('jump url', url)
        return url
    }
    /**
     * 处理映射链接参数
     * 
     */
    matchUrlRex(url, rule) {
        console.log('rule', rule)
        let _return = ''
        //调用原生
        if (rule.indexOf('callHandler') != -1) {
            let splitRes = rule.split('|'),
                method = splitRes[1],
                paramsKey = splitRes[2] ? splitRes[2].split(',') : [],
                params = {},
                urlQuery = this.getUrlQuery(url)
            paramsKey = paramsKey.filter(v => v);
            if (paramsKey.length > 0) {
                paramsKey.forEach(v => {
                    if (v.indexOf(':') != -1) {
                        let vSplit = v.split(':')
                        params[vSplit[0]] = urlQuery[vSplit[1]] || ''
                    } else if (v.indexOf('=') != -1) {
                        let vSplit = v.split('=')
                        params[vSplit[0]] = vSplit[1] || ''
                    } else {
                        params[v] = urlQuery[v] || ''
                    }
                })
            }
            console.log('callHandler', method, params)
            if (this.env == 'app') {
                window.WebViewJavascriptBridge.callHandler(method, params, function (response) {})
                return ''
            } else if (this.env == 'wxapp') {
                if (method == 'previewMedia') {
                    wx.previewMedia({
                        sources: [{
                            type: 'video',
                            ...params
                        }],
                        current: 0
                    })
                } else {
                    wx[method](params)
                }
                return ''
            }
        } else {
            let query = url.split('?')
            query = query[1] || ''
            if (rule.indexOf(':QM_') != -1) {
                let ruleSplit = rule.split(':QM_'),
                    QMSplit = ruleSplit[1].split(',')
                // console.log('QMSplit',QMSplit)
                QMSplit.forEach(v => {
                    if (v) {
                        let vSplit = v.split(':')
                        // query[vSplit[0]] = query[vSplit[1]] || '';
                        query = query.replace(vSplit[1] + '=', vSplit[0] + '=')

                    }
                })
                rule = ruleSplit[0]
            }
            // console.log('query',query)
            _return = rule.replace('${origin}', this.origin).replace('${query}', query)
        }
        return _return
    }

    /**
     * 跳转
     * @param urlOrEvent 链接地址 或者 小程序中的bindtap event
     * @param type app中传入的跳转类型
     */
    jumpTo(urlOrEvent, type) {
        console.log('urlOrEvent, type: ', urlOrEvent, type);
        // urlOrEvent = '/PLAYVIDEO?path=https%3A%2F%2Fwww.baidu.com%3Fa%3D23title=新年好&poster=https%3A%2F%2Fwww.baidu.com%3Fa%3D23'
        // urlOrEvent = '/coupon-list.html?id=1436207774919639041'
        // urlOrEvent = {
        //     currentTarget:{
        //         dataset:{
        //             url:'pages-main/index/index',
        //             opentype:'switchTab'
        //         }
        //     }
        // }
        if (this.env == 'uniApp' && (urlOrEvent.indexOf('.html') != -1 || urlOrEvent.indexOf('http') == 0)) {
            window.location.href = urlOrEvent
            return
        }
        if (this.env == 'wxapp' && !this.CONFIG) {
            this.CONFIG = getApp ? getApp().globalData.CONFIG : ''
        }
        let {
            url,
            openType,
            mustLogin
        } = this.getSourceUrlAndOpenType(urlOrEvent, type)
        if (!url) return
        if (mustLogin && !this.getStorage('auth-userinfo')) {
            url = this.env == 'wxapp' ? '/pages-native/auth/auth?next=back' : ToLogin()
        }
        url = this.mapUrl(url)
        if (!url) return
        console.log(url, openType)
        this[openType](url)
    }
}

export default Nav